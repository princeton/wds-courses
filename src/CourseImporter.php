<?php
namespace Drupal\pu_courses;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config;
use Drupal\pu_courses\Exception\CourseException;
/**
 * Class CourseImporter.
 *
 * @package Drupal\pu_courses
 */
class CourseImporter {
  protected $httpClient;
  protected $config;
  const COURSE_URL = "http://etcweb.princeton.edu/webfeeds/courseofferings/";
  /**
   * CourseImporter constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
    $this->config = \Drupal::config('pu_courses.settings');
  }
  /**
   * Download feed and save to cache.
   */
  public function refreshCache() {
    $data = $this->downloadFeed();
    \Drupal::cache()->set('pu_courses_term_data', $data, CacheBackendInterface::CACHE_PERMANENT);
    return $data;
  }
  /**
   * Get the data from cache.
   */
  protected function getDataFromCache() {
    if ($cache = \Drupal::cache()->get('pu_courses_term_data', TRUE)) {
      $data = $cache->data;
    }
    else {
      $data = $this->refreshCache();
    }
    return $data;
  }
  /**
   * Download the feed.
   */
  public function downloadFeed() {
    $subject = $this->config->get('subject');
    $show_latest = $this->config->get('show_latest');
    $subject_list = '';
    $term = 'latest';
    if ($show_latest == 1) {
      $term = 'latest';
    }
    else {
      $term = $this->config->get('term');
    }
    foreach ($subject as $value) {
      $subject_list = $subject_list . $value . ",";
    }
    $url = self::COURSE_URL . "?term=" . $this->config->get('term') . "&subject=" . $subject_list;
    try {
      $response = $this->httpClient->get($url, ['timeout' => 2]);
      if ($response->getStatusCode() == 200) {
        $data = $response->getBody();
        return ((string) $data);
      }
      else {
        throw new CourseException('Unexpected response status code: ' . $response->getStatusCode());
      }
    }
    catch (TransferException $e) {
      throw new CourseException($e);
    }
  }
  /**
   * Fetches the terms from the feed.
   */
  public function getTerm() {
    $opt = array();
    $url = "http://etcweb.princeton.edu/webfeeds/courseofferings/?term=list";
    $http_result = $this->httpClient->get($url, ['timeout' => 2]);
    try {
      if ($http_result->getStatusCode() == 200) {
        $data = (string) $http_result->getBody();
        $xml = new \SimpleXMLElement($data);
        $opt[''] = '--select one--';
        foreach ($xml->term as $item) {
          $opt[(string) $item->code] = (string) $item->reg_name;
        }
        return $opt;
      }
      else {
        throw new CourseException('Unexpected response status code: ' . $http_result->getStatusCode());
      }
    }
    catch (TransferException $e) {
      throw new CourseException($e);
    }
  }
  /**
   * Fetches the subjects from the feed.
   */
  public function getSubject($term = '') {
    $opt = array();
    $url = "http://etcweb.princeton.edu/webfeeds/courseofferings/" . "?subject=list&term=" . $term;
    $http_result = $this->httpClient->get($url, ['timeout' => 2]);
    if ($http_result->getStatusCode() == 200) {
      $data = (string) $http_result->getBody();
      $xml = new \SimpleXMLElement($data);
      foreach ($xml->term->subjects->subject as $item) {
        $opt[(string) $item->code] = (string) $item->name;
      }
      return $opt;
    }
    else {
      $msg = 'No content from %url.';
      $vars = array('%url' => $url);
      watchdog('Courses', $msg, $vars, WATCHDOG_WARNING);
      return t("Not accessible.");
    }
  }
  /**
   * To display courses.
   *
   * @param string $track
   *   track value can 'UGRD' or 'GRAD'.
   * @param bool $show_inst
   *   Show/hide instructor for courses.
   * @param bool $show_classes
   *   Show/hide description for courses.
   * @param bool $show_desc
   *   Show/hide description for courses.
   *
   * @return array|void
   *   Render array containing courses data.
   */
  public function displayCoursesByTrack($track, $show_inst, $show_classes, $show_desc) {
    $xml = new \SimpleXMLElement($this->getDataFromCache());
    $course_link = "http://registrar.princeton.edu/course-offerings/course_details.xml";
    $subjects = $xml->term->subjects->subject;
    $term = $xml->term->code;
    $header = $xml->term->cal_name;
    $data = array();
    $coursecount = 0;
    if (!empty($subjects)) {
      foreach ($subjects as $subject) {
        $subcode = $subject->code;
        $items = $subject->courses->course;
        foreach ($items as $item) {
          if ($item->detail->track == $track) {
            $name = (string) $subcode . " " . (string) $item->catalog_number;
            $instructors = $item->instructors->instructor;
            $cross = $item->crosslistings->crosslisting;
            $classes = $item->classes->class;
            $title = (string) $item->title;
            $id = (string) $item->course_id;
            $instructorlist = "";
            $instcount = 0;
            $coursecount += 1;
            $meetings = "";
            $link = $course_link . "?" . "courseid=" . (string) $item->course_id . "&term=" . $term;
            if ($cross) {
              foreach ($cross as $list) {
                $name = $name . "/" . $list->subject . " " . (string) $list->catalog_number;
              }
            }
            if ($show_desc == 1) {
              $description = (string) $item->detail->description;
            }
            else {
              $description = ' ';
            }
            if ($show_inst == 1) {
              if ($instructors) {
                foreach ($instructors as $instructor) {
                  $instcount += 1;
                  if ($instcount > 1) {
                    $instructorlist = $instructorlist . ", " . $instructor->first_name . " " . $instructor->last_name;
                  }
                  else {
                    $instructorlist = $instructorlist . $instructor->first_name . " " . $instructor->last_name;
                  }
                }
              }
            }
            if (($classes) && ($show_classes == 1)) {
              unset($meetings);
              foreach ($classes as $class) {
                foreach ($class->schedule->meetings->meeting as $meeting) {
                  $section = (string) $class->section;
                  $start_time = (string) $meeting->start_time;
                  $end_time = (string) $meeting->end_time;
                  $days = '';
                  foreach ($meeting->days->day as $day) {
                    $days = $days . $day . " ";
                  }
                  $meetings[] = array(
                    'section' => $section,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'days' => $days,
                  );
                }
              }
            }
            $data[] = array(
              'course_id' => $id,
              'course_name' => $name,
              'course_link' => $link,
              'course_title' => $title,
              'course_desc' => $description,
              'course_instructor' => $instructorlist,
              'course_classes' => $meetings,
            );
          }
        }
      }
//dsm($data);
    }
    return array('items' => $data, 'course_title' => $header);
  }
  /**
   * Display courses.
   *
   * @param string $show_inst
   *   Show/hide instructor.
   * @param bool $show_classes
   *   Show/hide classes.
   * @param bool $show_desc
   *   Show/hide description for courses.
   *
   * @return array
   *   return array.
   */
  public function displayCourses($show_inst, $show_classes, $show_desc) {
    $xml = new \SimpleXMLElement($this->getDataFromCache());
    $course_link = "http://registrar.princeton.edu/course-offerings/course_details.xml";
    $subjects = $xml->term->subjects->subject;
    $term = $xml->term->code;
    $header = $xml->term->cal_name;
    $data = array();
    $coursecount = 0;
    if (!empty($term)) {
      foreach ($subjects as $subject) {
        $subcode = $subject->code;
        $items = $subject->courses->course;
        foreach ($items as $item) {
          $name = (string) $subcode . " " . (string) $item->catalog_number;
          $instructors = $item->instructors->instructor;
          $cross = $item->crosslistings->crosslisting;
          $classes = $item->classes->class;
          $title = (string) $item->title;
          $id = (string) $item->course_id;
          $instructorlist = "";
          $instcount = 0;
          $coursecount += 1;
          $meetings = "";
          $link = $course_link . "?" . "courseid=" . (string) $item->course_id . "&term=" . $term;
          if ($cross) {
            foreach ($cross as $list) {
              $name = $name . "/" . $list->subject . " " . (string) $list->catalog_number;
            }
          }
          if ($show_desc == 1) {
            $description = (string) $item->detail->description;
          }
          else {
            $description = ' ';
          }
          if ($show_inst == 1) {
            if ($instructors) {
              foreach ($instructors as $instructor) {
                $instcount += 1;
                if ($instcount > 1) {
                  $instructorlist = $instructorlist . ", " . $instructor->first_name . " " . $instructor->last_name;
                }
                else {
                  $instructorlist = $instructorlist . $instructor->first_name . " " . $instructor->last_name;
                }
              }
            }
          }
          if (($classes) && ($show_classes == 1)) {
            unset($meetings);
            foreach ($classes as $class) {
              foreach ($class->schedule->meetings->meeting as $meeting) {
                $section = (string) $class->section;
                $start_time = (string) $meeting->start_time;
                $end_time = (string) $meeting->end_time;
                $days = '';
                foreach ($meeting->days->day as $day) {
                  $days = $days . $day . " ";
                }
                $meetings[] = array(
                  'section' => $section,
                  'start_time' => $start_time,
                  'end_time' => $end_time,
                  'days' => $days,
                );
              }
            }
          }
          $data[] = array(
            'course_id' => $id,
            'course_name' => $name,
            'course_link' => $link,
            'course_title' => $title,
            'course_desc' => $description,
            'course_instructor' => $instructorlist,
            'course_classes' => $meetings,
          );
        }
      }
    }
    return array('items' => $data, 'course_title' => $header);
  }
}