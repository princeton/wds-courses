<?php
namespace Drupal\pu_courses\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pu_courses\CourseImporter;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Configure settings Term Courses.
 */
class CourseTermSettingForm extends ConfigFormBase {
  protected $courseImporter;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CourseImporter $courseImporter) {
    parent::__construct($config_factory);
    $this->courseImporter = $courseImporter;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('course_importer')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pu_course.settings';
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pu_courses.settings',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pu_courses.settings');
    $termOpt = $this->courseImporter->getTerm();
    $subject = $this->courseImporter->getSubject('latest');
    $form['pu_courses_latest_term'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Latest Available Term'),
      '#description' => t('If you check this box, the courses will update automatically when the Registrar\'s information is updated.'),
      '#default_value' => $config->get('show_latest'),
    );
    $form['pu_courses_term_select'] = array(
      '#title' => t("Select the term"),
      '#description' => ("If you choose a specific semester, it will not update automatically to the latest available term. You will need to update manually. The semester you’ve picked will only be available for 3-4 years."),
      '#type' => 'select',
      '#prefix' => "<div id='dropdown_term_replace'>",
      '#suffix' => "</div>",
      '#options' => $termOpt,
      '#default_value' => $config->get('term'),
    );
    $form['pu_courses_subject'] = array(
      '#title' => t("Select the Subject (Use Ctrl-Click (or Cmd-Click on Mac) to select multiple subjects)"),
      '#type' => 'select',
      '#prefix' => '<div id="dropdown_subjects_replace">',
      '#suffix' => '</div>',
      '#options' => $subject,
      '#required' => TRUE,
      '#default_value' => $config->get('subject'),
      '#multiple' => TRUE,
      '#size' => 15,
    );
    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('pu_courses.settings')
      ->set('show_latest', $form_state->getValue('pu_courses_latest_term'))
      ->set('term', $form_state->getValue(['pu_courses_term_select']))
      ->set('subject', $form_state->getValue('pu_courses_subject'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
