<?php

namespace Drupal\pu_courses\Exception;

/**
 * Class CourseException.
 */
class CourseException extends \RuntimeException {
}
