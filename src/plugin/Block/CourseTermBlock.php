<?php
namespace Drupal\pu_courses\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pu_courses\CourseImporter;
use Drupal\pu_courses\Exception\CourseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
/**
 * Provides a 'Course' Block.
 *
 * @Block(
 *   id = "course_term_block",
 *   admin_label = @Translation("Course for a term block"),
 * )
 */
class CourseTermBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The Course Importer.
   *
   * @var \Drupal\pu_courses\courseImporter
   */
  protected $courseImporter;
  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;
  /**
   * CourseTermBlock constructor.
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\pu_courses\CourseImporter $course_importer
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CourseImporter $course_importer, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->courseImporter = $course_importer;
    $this->logger = $logger;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('course_importer'),
      $container->get('logger.channel.pu_courses')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    $this->setConfigurationValue('track', $form_state->getValue('track'));
    $this->setConfigurationValue('show_inst', $form_state->getValue('show_inst'));
    $this->setConfigurationValue('show_classes', $form_state->getValue('show_classes'));
    $this->setConfigurationValue('show_desc', $form_state->getValue('show_desc'));
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $track = isset($config['track']) ? $config['track'] : 'empty';
    $show_inst = isset($config['show_inst']) ? $config['show_inst'] : 'empty';
    $show_classes = isset($config['show_classes']) ? $config['show_classes'] : 'empty';
    $show_desc = isset($config['show_desc']) ? $config['show_desc'] : 'empty';
    try {
      if ($track == 'all') {
        $arr = $this->courseImporter->displayCourses($show_inst, $show_classes, $show_desc);
      }
      else {
        $arr = $this->courseImporter->displayCoursesByTrack($track, $show_inst, $show_classes, $show_desc);
      }
    }
    catch (CourseException $e) {
      $this->logger->error('CourseException encountered when displaying course term block: %error', ['%error' => $e->getMessage()]);
      return array(
        '#markup' => '<p>Error encountered.</p>',
      );
    }
    if (empty($arr)) {
      return array(
        '#markup' => '<p>No course data found.</p>',
      );
    }
    else {
      return array(
        '#theme' => 'pu_courses',
        '#items' => $arr,
      );
    }
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['track'] = [
      '#title' => $this->t('Select the Track'),
      '#description' => $this->t('Select the track to display courses only for that Track.'),
      '#type' => 'radios',
      '#options' =>
        array(
          'UGRD' => 'Undergraduate',
          'GRAD' => 'Graduate',
          'all' => 'Both Undergraduate and Graduate together'
        ),
      '#required' => FALSE,
      '#default_value' => isset($this->configuration['track']) ? $this->configuration['track'] : 'UGRD',
    ];
    $form['what_do_you_want'] = [
      '#title' => $this->t('What do you want to show:'),
      '#type' => 'label',
    ];
    $form['show_classes'] = [
      '#title' => $this->t('Show Classes'),
      '#type' => 'checkbox',
      '#default_value' => isset($this->configuration['show_classes']) ? $this->configuration['show_classes'] : 0,
    ];
    $form['show_desc'] = [
      '#title' => $this->t('Show Course Description'),
      '#type' => 'checkbox',
      '#default_value' => isset($this->configuration['show_desc']) ? $this->configuration['show_desc'] : 0,
    ];
    $form['show_inst'] = [
      '#title' => $this->t('Show Instructor'),
      '#type' => 'checkbox',
      '#default_value' => isset($this->configuration['show_inst']) ? $this->configuration['show_inst'] : 0,
    ];
    return $form;
  }
}
